/*
    &lt;script type="text/javascript"&gt;
    [this]
    &lt;/script&gt;
*/

let expr_element = document.getElementById(
    "expr-target"
);

let btn_element = document.getElementById(
    "btn-comp"
);

let res_element = document.getElementById(
    "div-result"
);

let chars_ok = "0123456789 +-*/%()";

btn_element.onclick = function() {
    let expr_captured_raw = (
        expr_element.innerHTML
    ).replace("<br>", "");
    for (const i of expr_captured_raw) {
        if (!chars_ok.includes(i)) {
            throw 'Invalid Character';
        }
    }
    let q_param = encodeURIComponent(
        encodeURIComponent(
            expr_captured_raw
        )
    );
    res_element.innerHTML = (
        `
        <iframe
            src="/mafs/${q_param}"
        >
        </iframe>
        `
    );
};
