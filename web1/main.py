from flask import Flask
import urllib.parse
import os

app = Flask('app')
process_text = lambda x: \
  proc_text(x.replace("<br>", ""))
proc_text = eval
host_ip = '0.0.0.0'
host_port_gateway = '8080'
host_port = host_port_gateway

@app.route('/')
def route_index():
    with open("index_view.html") as f:
        with open("index.js") as f_js:
            return f.read().format(f_js.read())

@app.route('/mafs/<path:text>')
def route_calculate(text):
    decoded = urllib.parse.unquote(text)
    return f'''
    <h2>Answer: {process_text(decoded)}</h2>
    '''

os.system('echo "Running @ $(pwd)"')

app.run(host= host_ip, port= host_port)
